type CarYear = number;
type CarType = string;
type CarModel = String;

type Car = {
    year: CarYear,
    type: CarType,
    mode: CarModel
}
const CarYear: CarYear = 2001;
const CarType: CarType = "Toyota";
const CarModel: CarModel = "Corolla";

const car1: Car ={
    year: CarYear,
    type: CarType,
    mode: CarModel
}
console.log(car1);