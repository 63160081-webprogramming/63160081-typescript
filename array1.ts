const names : String[] = [];
names.push("Dylan");
names.push("Suphakorn");
console.log(names[0]);
console.log(names[1]);
console.log(names.length);


for(let i=0 ; i < names.length; i++){
    console.log(names[i]);
}

for(let index in names){
    console.log(names[index]);
}

names.forEach(function(name){
    console.log(name);
});