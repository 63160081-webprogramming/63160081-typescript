interface Rectangle {
    width: number,
    heigh: number,
}

interface ColoredRectangle extends Rectangle{
    color: String
}

const rectangle: Rectangle ={
    width: 20,
    heigh: 10
}

console.log(rectangle); 

const coloredRectangle: ColoredRectangle = {
    width: 20,
    heigh: 10,
    color: "red"
}

console.log(coloredRectangle); 

